// load column mapping options when a new dataset is loaded

Shiny.addCustomMessageHandler("new_data",
function(cols) {
    console.log("Setting cols: " + JSON.stringify(cols));
    var ecols = document.getElementById("select_entity");
    var tcols = document.getElementById("select_time");
    var dcols = document.getElementById("select_data");
    var opts = "";

    opts = "";
    for (var i = 0; i < cols.length; i++){
	opts += "<option value='" + cols[i] + "'" + (i == 0 ? "selected" : "") + ">" + cols[i] + "</option>";
    }

    ecols.innerHTML = opts;

    opts = "";
    for (var i = 0; i < cols.length; i++){
	opts += "<option value='" + cols[i] + "'" + (i == 1 ? "selected" : "") + ">" + cols[i] + "</option>";
    }
    tcols.innerHTML = opts;

    opts = "";
    for (var i = 0; i < cols.length; i++){
	opts += "<option value='" + cols[i] + "'" + (i > 1 ? "selected" : "") + ">" + cols[i] + "</option>";
    }
    dcols.innerHTML = opts;
}
			     );

update_selex = function(){
    console.log("update_selex");

    var sels = ["select_entity", "select_time", "select_data"];

    for (i = 0; i < sels.length; i++){
	var sname = sels[i];
	var a = document.getElementById(sname);

	// find selected options
	var s = [];
	for (o in a.options){
	    console.log("checking option: " + o.value);
	    if (o.selected){
		console.log("selval: " + o.value);
		s = [s, o.value];
	    }
	}

	console.log("in " + sname + " found selected: " + s);

	// deselect them in other dropdowns
	for (j = 0; j < sels.length; j++){
	    if (i != j){
		var subname = sels[j];
		var b = document.getElementById(subname);

		for (o in b.options){
		    if (s.indexOf(o.value) > -1){
			o.selected = false;
			console.log("deselect " + o.value + " in " + subname);
		    }else{
			console.log("maintain " + o.value + " in " + subname);
		    }
		}
	    }
	}
    }
}

// keep columns mutually exclusive
$('#select_entity').change(update_selex);
$('#select_time').change(update_selex);
$('#select_data').change(update_selex);
