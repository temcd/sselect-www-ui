var help = {
    data : function(){
	show_help("Chose a file with panel data in CSV format. The data must contain one column indexing your measured entities, one column for time and an arbitrary number of columns representing measured properties.");
    },

    numbootstrap: function(){
	show_help("Bootstraps of the dataset are used to Monte Carlo estimate the selection probabilities of model components. A higher number of bootstraps will increase the vertical resolution and stability of the graph.");
    },

    typebootstrap: function(){
	show_help("<span style='font-weight: bold;'>resample series</span>: simplest method, resamples complete series.<br/><span style='font-weight: bold;'>resample dynamics</span>: build new data by approximating a markov kernel from the steps in the data.");
    },

    regularisation: function(){
	show_help("The estimated selection probabilities of model components are tested at a number of regularisation steps, which constitute the horizontal resolution of the graph.");
    },

    modelorder: function(){
	show_help("The system underlying the data is modeled as a system of SDEs with polynomial drift functions with interaction terms up to a maximal degree. Chosing a higher degree allows representing a more strongly non-linear system, but drastically increases the number of model terms and thereby also the computations and memory required.");
    },

    derivmodel: function(){
	show_help("The regression models approximated time derivatives from the series. For a fast analysis, use the 'step' method for these approximations. Using the 'poly' method works better with non-linear curvatures, but takes significantly longer.");
    },

    datacols: function(){
	show_help("Here you need to chose which columns in your data represent the index of the series, the time value and the measured values.");
    },

    indicated: function(){
	show_help("Below are listed indicated terms for the different models representing the dimensions of the system. A term is considered to be indicated if it's estimated selection probability is greater than the chosen probability limit anywhere above the chosen regularisation limit, as indicated by the cross in each model's graph. These terms are also shown as solid lines in the graph.");
    }, 

    about: function(){
	show_help("This implementation of stability selection is made for analysing multiple time series and produces polynomial stochastic differential equation (SDE) models representing possible trends common to the series. The underlying software was developed during my master <a href='http://www.diva-portal.org/smash/get/diva2:750443/FULLTEXT01.pdf'>thesis</a> project together with Tilo Wiklund.");
    }

};

function show_help(v){
    var pop = document.getElementById("popup");
    var pop_cont = document.getElementById("popup_content");
    pop.style.zIndex = 1;
    pop_cont.innerHTML = v;
    pop.style.visibility = "visible";
}

function close_help(){
    var pop = document.getElementById("popup");
    pop.style.zIndex = -2;
    pop.style.visibility = "hidden";
}
